# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gst-plugins-good-1.0 package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gst-plugins-good-1.24.12\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-29 20:13+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ext/jack/gstjackaudiosink.c:360 ext/jack/gstjackaudiosrc.c:365
msgid "Jack server not found"
msgstr ""

#: ext/jpeg/gstjpegdec.c:983 ext/jpeg/gstjpegdec.c:1260
#: ext/jpeg/gstjpegdec.c:1269 ext/jpeg/gstjpegdec.c:1279
#: ext/jpeg/gstjpegdec.c:1288 ext/jpeg/gstjpegdec.c:1580
#: ext/jpeg/gstjpegdec.c:1608
msgid "Failed to decode JPEG image"
msgstr ""

#: ext/jpeg/gstjpegdec.c:1562
msgid "Failed to read memory"
msgstr ""

#: ext/lame/gstlamemp3enc.c:407
msgid ""
"Failed to configure LAME mp3 audio encoder. Check your encoding parameters."
msgstr ""

#. <php-emulation-mode>three underscores for ___rate is really really really
#. * private as opposed to one underscore<php-emulation-mode>
#. call this MACRO outside of the NULL state so that we have a higher chance
#. * of actually having a pipeline and bus to get the message through
#: ext/lame/gstlamemp3enc.c:439 ext/twolame/gsttwolamemp2enc.c:494
#, c-format
msgid ""
"The requested bitrate %d kbit/s for property '%s' is not allowed. The "
"bitrate was changed to %d kbit/s."
msgstr ""

#. TRANSLATORS: 'song title' by 'artist name'
#: ext/pulse/pulsesink.c:3132
#, c-format
msgid "'%s' by '%s'"
msgstr ""

#: ext/shout2/gstshout2.c:717 ext/shout2/gstshout2.c:727
msgid "Could not connect to server"
msgstr ""

#: ext/soup/gstsouphttpsrc.c:1145
msgid "No URL set."
msgstr ""

#: ext/soup/gstsouphttpsrc.c:1664
msgid "Could not resolve server name."
msgstr ""

#: ext/soup/gstsouphttpsrc.c:1669
msgid "Could not establish connection to server."
msgstr ""

#: ext/soup/gstsouphttpsrc.c:1673
msgid "Secure connection setup failed."
msgstr ""

#: ext/soup/gstsouphttpsrc.c:1679
msgid ""
"A network error occurred, or the server closed the connection unexpectedly."
msgstr ""

#: ext/soup/gstsouphttpsrc.c:1684
msgid "Server sent bad data."
msgstr ""

#: ext/soup/gstsouphttpsrc.c:1984
msgid "Server does not support seeking."
msgstr ""

#: ext/twolame/gsttwolamemp2enc.c:417
msgid "Failed to configure TwoLAME encoder. Check your encoding parameters."
msgstr ""

#: gst/avi/gstavimux.c:1946
msgid "No or invalid input audio, AVI stream will be corrupt."
msgstr ""

#: gst/isomp4/qtdemux.c:508 gst/isomp4/qtdemux.c:513
msgid "This file contains no playable streams."
msgstr ""

#: gst/isomp4/qtdemux.c:559 gst/isomp4/qtdemux.c:8013 gst/isomp4/qtdemux.c:8082
#: gst/isomp4/qtdemux.c:8398 gst/isomp4/qtdemux.c:9783
msgid "This file is invalid and cannot be played."
msgstr ""

#: gst/isomp4/qtdemux.c:3071
msgid "Cannot play stream because it is encrypted with PlayReady DRM."
msgstr ""

#: gst/isomp4/qtdemux.c:4727 gst/isomp4/qtdemux.c:9175
#: gst/isomp4/qtdemux.c:9182 gst/isomp4/qtdemux.c:10507
#: gst/isomp4/qtdemux.c:10949 gst/isomp4/qtdemux.c:10956
#: gst/isomp4/qtdemux.c:14275
msgid "This file is corrupt and cannot be played."
msgstr ""

#: gst/isomp4/qtdemux.c:4969
msgid "Invalid atom size."
msgstr ""

#: gst/isomp4/qtdemux.c:5022 gst/isomp4/qtdemux.c:5029
msgid "Cannot query file size"
msgstr ""

#: gst/isomp4/qtdemux.c:5038
msgid "Cannot demux file"
msgstr ""

#: gst/isomp4/qtdemux.c:5078
msgid "This file is incomplete and cannot be played."
msgstr ""

#: gst/isomp4/qtdemux.c:12167
msgid "The video in this file might not play correctly."
msgstr ""

#: gst/rtsp/gstrtspsrc.c:8074
msgid ""
"No supported stream was found. You might need to install a GStreamer RTSP "
"extension plugin for Real media streams."
msgstr ""

#: gst/rtsp/gstrtspsrc.c:8079
msgid ""
"No supported stream was found. You might need to allow more transport "
"protocols or may otherwise be missing the right GStreamer RTSP extension "
"plugin."
msgstr ""

#: sys/oss4/oss4-sink.c:495 sys/oss4/oss4-source.c:360 sys/oss/gstosssink.c:387
msgid ""
"Could not open audio device for playback. Device is being used by another "
"application."
msgstr ""

#: sys/oss4/oss4-sink.c:505 sys/oss4/oss4-source.c:370 sys/oss/gstosssink.c:394
msgid ""
"Could not open audio device for playback. You don't have permission to open "
"the device."
msgstr ""

#: sys/oss4/oss4-sink.c:516 sys/oss4/oss4-source.c:381 sys/oss/gstosssink.c:402
msgid "Could not open audio device for playback."
msgstr ""

#: sys/oss4/oss4-sink.c:525 sys/oss4/oss4-source.c:391
msgid ""
"Could not open audio device for playback. This version of the Open Sound "
"System is not supported by this element."
msgstr ""

#: sys/oss4/oss4-sink.c:648
msgid "Playback is not supported by this audio device."
msgstr ""

#: sys/oss4/oss4-sink.c:655
msgid "Audio playback error."
msgstr ""

#: sys/oss4/oss4-source.c:505
msgid "Recording is not supported by this audio device."
msgstr ""

#: sys/oss4/oss4-source.c:512
msgid "Error recording from audio device."
msgstr ""

#: sys/oss/gstosssrc.c:379
msgid ""
"Could not open audio device for recording. You don't have permission to open "
"the device."
msgstr ""

#: sys/oss/gstosssrc.c:387
msgid "Could not open audio device for recording."
msgstr ""

#: sys/osxaudio/gstosxaudioringbuffer.c:149
msgid "CoreAudio device not found"
msgstr ""

#: sys/osxaudio/gstosxaudioringbuffer.c:155
msgid "CoreAudio device could not be opened"
msgstr ""

#: sys/rpicamsrc/gstrpicamsrcdeviceprovider.c:148
msgid "Raspberry Pi Camera Module"
msgstr ""

#: sys/v4l2/gstv4l2bufferpool.c:1870
#, c-format
msgid "Error reading %d bytes from device '%s'."
msgstr ""

#: sys/v4l2/gstv4l2object.c:1313
#, c-format
msgid "Failed to enumerate possible video formats device '%s' can work with"
msgstr ""

#: sys/v4l2/gstv4l2object.c:3238
#, c-format
msgid "Could not map buffers from device '%s'"
msgstr ""

#: sys/v4l2/gstv4l2object.c:3246
#, c-format
msgid "The driver of device '%s' does not support the IO method %d"
msgstr ""

#: sys/v4l2/gstv4l2object.c:3253
#, c-format
msgid "The driver of device '%s' does not support any known IO method."
msgstr ""

#: sys/v4l2/gstv4l2object.c:4278
msgid "Invalid caps"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4285 sys/v4l2/gstv4l2object.c:4309
#, c-format
msgid "Device '%s' has no supported format"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4291 sys/v4l2/gstv4l2object.c:4315
#, c-format
msgid "Device '%s' failed during initialization"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4303
#, c-format
msgid "Device '%s' is busy"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4326
#, c-format
msgid "Device '%s' cannot capture at %dx%d"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4335
#, c-format
msgid "Device '%s' cannot capture in the specified format"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4346
#, c-format
msgid "Device '%s' does support non-contiguous planes"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4361
#, c-format
msgid "Device '%s' does not support %s interlacing"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4375
#, c-format
msgid "Device '%s' does not support %s colorimetry"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4387
#, c-format
msgid "Could not get parameters on device '%s'"
msgstr ""

#: sys/v4l2/gstv4l2object.c:4395
msgid "Video device did not accept new frame rate setting."
msgstr ""

#: sys/v4l2/gstv4l2object.c:4538
msgid "Video device did not provide output format."
msgstr ""

#: sys/v4l2/gstv4l2object.c:4544
msgid "Video device returned invalid dimensions."
msgstr ""

#: sys/v4l2/gstv4l2object.c:4552
msgid "Video device uses an unsupported interlacing method."
msgstr ""

#: sys/v4l2/gstv4l2object.c:4559
msgid "Video device uses an unsupported pixel format."
msgstr ""

#: sys/v4l2/gstv4l2object.c:5504
msgid "Failed to configure internal buffer pool."
msgstr ""

#: sys/v4l2/gstv4l2object.c:5510
msgid "Video device did not suggest any buffer size."
msgstr ""

#: sys/v4l2/gstv4l2object.c:5516
msgid "No downstream pool to import from."
msgstr ""

#: sys/v4l2/gstv4l2radio.c:144
#, c-format
msgid "Failed to get settings of tuner %d on device '%s'."
msgstr ""

#: sys/v4l2/gstv4l2radio.c:151
#, c-format
msgid "Error getting capabilities for device '%s'."
msgstr ""

#: sys/v4l2/gstv4l2radio.c:158
#, c-format
msgid "Device '%s' is not a tuner."
msgstr ""

#: sys/v4l2/gstv4l2radio.c:185
#, c-format
msgid "Failed to get radio input on device '%s'. "
msgstr ""

#: sys/v4l2/gstv4l2radio.c:208
#, c-format
msgid "Failed to set input %d on device %s."
msgstr ""

#: sys/v4l2/gstv4l2radio.c:242
#, c-format
msgid "Failed to change mute state for device '%s'."
msgstr ""

#: sys/v4l2/gstv4l2sink.c:636
msgid "Failed to allocated required memory."
msgstr ""

#: sys/v4l2/gstv4l2src.c:957 sys/v4l2/gstv4l2videodec.c:524
#: sys/v4l2/gstv4l2videodec.c:1062 sys/v4l2/gstv4l2videoenc.c:900
msgid "Failed to allocate required memory."
msgstr ""

#: sys/v4l2/gstv4l2transform.c:144
#, c-format
msgid "Converter on device %s has no supported input format"
msgstr ""

#: sys/v4l2/gstv4l2transform.c:151
#, c-format
msgid "Converter on device %s has no supported output format"
msgstr ""

#: sys/v4l2/gstv4l2videodec.c:149
#, c-format
msgid "Decoder on device %s has no supported input format"
msgstr ""

#: sys/v4l2/gstv4l2videodec.c:1076
msgid "Failed to start decoding thread."
msgstr ""

#: sys/v4l2/gstv4l2videodec.c:1083 sys/v4l2/gstv4l2videoenc.c:921
msgid "Failed to process frame."
msgstr ""

#: sys/v4l2/gstv4l2videoenc.c:151
#, c-format
msgid "Encoder on device %s has no supported output format"
msgstr ""

#: sys/v4l2/gstv4l2videoenc.c:158
#, c-format
msgid "Encoder on device %s has no supported input format"
msgstr ""

#: sys/v4l2/gstv4l2videoenc.c:865
msgid "Failed to force keyframe."
msgstr ""

#: sys/v4l2/gstv4l2videoenc.c:913
msgid "Failed to start encoding thread."
msgstr ""

#: sys/v4l2/v4l2_calls.c:92
#, c-format
msgid ""
"Error getting capabilities for device '%s': It isn't a v4l2 driver. Check if "
"it is a v4l1 driver."
msgstr ""

#: sys/v4l2/v4l2_calls.c:156
#, c-format
msgid "Failed to query attributes of input %d in device %s"
msgstr ""

#: sys/v4l2/v4l2_calls.c:188
#, c-format
msgid "Failed to get setting of tuner %d on device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:236
#, c-format
msgid "Failed to query norm on device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:422
#, c-format
msgid "Failed getting controls attributes on device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:614
#, c-format
msgid "Cannot identify device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:621
#, c-format
msgid "This isn't a device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:628
#, c-format
msgid "Could not open device '%s' for reading and writing."
msgstr ""

#: sys/v4l2/v4l2_calls.c:635
#, c-format
msgid "Device '%s' is not a capture device."
msgstr ""

#: sys/v4l2/v4l2_calls.c:642
#, c-format
msgid "Device '%s' is not a output device."
msgstr ""

#: sys/v4l2/v4l2_calls.c:649
#, c-format
msgid "Device '%s' is not a M2M device."
msgstr ""

#: sys/v4l2/v4l2_calls.c:701
#, c-format
msgid "Could not dup device '%s' for reading and writing."
msgstr ""

#: sys/v4l2/v4l2_calls.c:785
#, c-format
msgid "Failed to set norm for device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:823
#, c-format
msgid "Failed to get current tuner frequency for device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:865
#, c-format
msgid "Failed to set current tuner frequency for device '%s' to %lu Hz."
msgstr ""

#: sys/v4l2/v4l2_calls.c:899
#, c-format
msgid "Failed to get signal strength for device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:935
#, c-format
msgid "Failed to get value for control %d on device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:970
#, c-format
msgid "Failed to set value %d for control %d on device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:1030
#, c-format
msgid "Failed to set value %s for control %d on device '%s'."
msgstr ""

#: sys/v4l2/v4l2_calls.c:1119
#, c-format
msgid "Failed to get current input on device '%s'. May be it is a radio device"
msgstr ""

#: sys/v4l2/v4l2_calls.c:1144
#, c-format
msgid "Failed to set input %u on device %s."
msgstr ""

#: sys/v4l2/v4l2_calls.c:1191
#, c-format
msgid ""
"Failed to get current output on device '%s'. May be it is a radio device"
msgstr ""

#: sys/v4l2/v4l2_calls.c:1216
#, c-format
msgid "Failed to set output %u on device %s."
msgstr ""

#: sys/ximage/gstximagesrc.c:873
msgid "Cannot operate without a clock"
msgstr ""
